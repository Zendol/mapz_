﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class Optimizer
    {
		
		public bool isWhileExpr { get; set; }

		public Optimizer(bool wExpr = false)
		{
			isWhileExpr = wExpr;
		}

		bool IsConstantValueOf(IExpression node, double value, Context context)
		{
			return (IsConstant(node, context, out double constantValue) && constantValue == value);
		}

		bool IsZero(IExpression node, Context variableTable)
		{
			if (node is VariableExpression)
				return false;
			return IsConstantValueOf(node, 0.0, variableTable);
		}

		bool IsOne(IExpression node, Context variableTable)
		{
			return IsConstantValueOf(node, 1.0, variableTable);
		}

		bool IsNaN(IExpression node, Context variableTable)
		{
			return IsConstantValueOf(node, Double.NaN, variableTable);
		}

		bool IsConstant(IExpression node, Context variableTable, out double constantValue)
		{
			if (node is NumberExpression lNode)
			{
				constantValue = lNode.value;
				return true;
			}
			else if (node is VariableExpression viChild && variableTable != null && variableTable.variables.Count > 0)
			{
				constantValue = variableTable.variables[viChild.name];
				return true;
			}
			else
			{
				constantValue = Double.NaN;
				return false;
			}
		}

		void Optimize(ref IExpression node, Context variableTable, Func<IExpression, Context, IExpression> checkFunction)
		{
			switch (node)
			{
				case BinaryExpression bNode:
					IExpression leftChild = bNode.expr1;
					IExpression rightChild = bNode.expr2;
					if(bNode is AssignmentExpression)
					{ 
						Optimize(ref rightChild, variableTable, checkFunction);
					}
					else
					{
						Optimize(ref leftChild, variableTable, checkFunction);
						Optimize(ref rightChild, variableTable, checkFunction);
					}
					bNode.expr1 = leftChild;
					bNode.expr2 = rightChild;
					break;

				case UnaryExpression uNode:
					IExpression child = uNode.expr1;
					Optimize(ref child, variableTable, checkFunction);
					uNode.expr1 = child;
					break;
				default: break;
			}

			node = checkFunction(node, variableTable);
		}

		IExpression CalculateConstant(IExpression node, Context variableTable)
		{

			if (IsConstant(node, variableTable, out double constantOperand))
			{
				return new NumberExpression(constantOperand);
			}

			/*if (node is BinaryExpression bNode && !(bNode is AssignmentExpression))
			{
				if (IsConstant(bNode.expr1, variableTable, out double constantLeftOperand)
				&& IsConstant(bNode.expr2, variableTable, out double constantRightOperand))
				{
					bNode.expr1 = new NumberExpression(constantLeftOperand);
					bNode.expr2 = new NumberExpression(constantRightOperand);
					double preCalculatedValue = Convert.ToDouble(bNode.Interpret(variableTable));
					return new NumberExpression(preCalculatedValue);
				}
			}

			if (node is UnaryExpression uNode)
			{
				if (IsConstant(uNode.expr1, variableTable, out double constantLeftOperand))
				{
					return new NumberExpression(constantLeftOperand);
				}
			}*/

				return node;
		}

		IExpression RemoveMultiplicationByZero(IExpression node, Context variableTable)
		{
			return (node is BinaryExpression bTreeNode && bTreeNode is MultiplyExpression &&
				(IsZero(bTreeNode.expr1, variableTable) || IsZero(bTreeNode.expr2, variableTable)))
				? new NumberExpression(0)
				: node;
		}

		IExpression RemoveMultiplicationByOne(IExpression node, Context variableTable)
		{
			if (node is BinaryExpression bTreeNode && bTreeNode is MultiplyExpression)
			{
				return IsOne(bTreeNode.expr1, variableTable) ?  bTreeNode.expr2
					: IsOne(bTreeNode.expr2, variableTable) ? bTreeNode.expr1
					: bTreeNode;
			}

			return node;
		}

		IExpression RemoveUnneccaryAssignment(IExpression node, Context variableTable)
		{
			if (node is BinaryExpression bTreeNode && bTreeNode is AssignmentExpression)
			{
				if (Convert.ToDouble(bTreeNode.expr1.Interpret(variableTable)) == 
					Convert.ToDouble(bTreeNode.expr2.Interpret(variableTable)))
					return null;
				else
					return node;
				
			}
			return node;
		}
	

		IExpression RemoveZeroAdditionOrSubstraction(IExpression node, Context variableTable)
		{
			if (node is BinaryExpression bTreeNode
				&& (bTreeNode is PlusExpression 
				|| bTreeNode is MinusExpression))
			{
				return IsZero(bTreeNode.expr1, variableTable) ? bTreeNode.expr2
					: IsZero(bTreeNode.expr2, variableTable) ? bTreeNode.expr1
					: node;
			}

			return node;
		}



		


	

		


		public IExpression OptimizeWithTable(IExpression unoptimized, Context context)
		{
			IExpression optimizedTree = unoptimized;
			Context variableTable = context;
				if(!(unoptimized is AssignmentExpression) && !(isWhileExpr))
					Optimize(ref optimizedTree, variableTable, CalculateConstant);
				Optimize(ref optimizedTree, variableTable, RemoveMultiplicationByZero);
				Optimize(ref optimizedTree, variableTable, RemoveMultiplicationByOne);
				Optimize(ref optimizedTree, variableTable, RemoveZeroAdditionOrSubstraction);
				if (!(unoptimized is VarExpression))
					Optimize(ref optimizedTree, variableTable, RemoveUnneccaryAssignment);
			

			return optimizedTree;
	
		}
	}
}

