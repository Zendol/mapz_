﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class VarExpression : UnaryExpression
    {
        public override object Interpret(Context context)
        {
            double res = Convert.ToDouble(expr1.Interpret(context));
            return res;
        }
    }
}
