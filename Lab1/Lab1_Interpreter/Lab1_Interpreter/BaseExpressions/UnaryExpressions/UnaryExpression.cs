﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public abstract class UnaryExpression : IExpression
    {
        public IExpression expr1;

        public virtual PRIORITY Priority { get { return PRIORITY.ZERO; } }
        public abstract object Interpret(Context context);
    }
}
