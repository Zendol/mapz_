﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Lab1_Interpreter
{
    public class CursorMoveExpression : BinaryExpression
    {
        public override PRIORITY Priority { get { return PRIORITY.TWO; } }
        public override object Interpret(Context context)
        {
            int x = Convert.ToInt32(expr1.Interpret(context));
            int y = Convert.ToInt32(expr2.Interpret(context));
            MouseOperations.SetCursorPosition(x,y);
            return null;
        }
    }
}
