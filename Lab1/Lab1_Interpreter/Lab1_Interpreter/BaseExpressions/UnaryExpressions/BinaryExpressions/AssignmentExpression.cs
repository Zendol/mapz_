﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Lab1_Interpreter
{
    public class AssignmentExpression : BinaryExpression
    {

        public override object Interpret(Context context)
        {

                expr1.Interpret(context);
                VariableExpression variable = expr1 as VariableExpression;
                
        
                double value = Convert.ToDouble(expr2.Interpret(context));

                context.SetVariable(variable.name, value);
                return value;
        }
    }
}
