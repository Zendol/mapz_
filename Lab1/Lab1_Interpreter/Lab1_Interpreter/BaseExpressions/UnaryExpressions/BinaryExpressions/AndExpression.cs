﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class AndExpression : BinaryExpression
    {
        public override PRIORITY Priority { get { return PRIORITY.ONE; } }
        public override object Interpret(Context context)
        {
            return Convert.ToBoolean(expr1.Interpret(context)) && Convert.ToBoolean(expr2.Interpret(context));

        }
    }
}
