﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class MultiplyExpression : BinaryExpression
    {
         public override PRIORITY Priority { get { return PRIORITY.FOUR; }  }
        public override object Interpret(Context context)
        {
            return Convert.ToDouble(expr1.Interpret(context)) * Convert.ToDouble(expr2.Interpret(context));

        }
    }
}
