﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class ObjectMoveExpression : BinaryExpression
    {

       public override PRIORITY Priority { get { return PRIORITY.TWO; } }
       public override object Interpret(Context context)
       {
      
           int x = Convert.ToInt32(expr1.Interpret(context));
           int y = Convert.ToInt32(expr2.Interpret(context));
           MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
           MouseOperations.SetCursorPosition(x, y);
           MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
           //Thread.Sleep(100);
           return null;
       }
    

    }
}
