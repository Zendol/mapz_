﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public abstract class  BinaryExpression:UnaryExpression
    {
        public IExpression expr2;
         public override PRIORITY Priority { get { return PRIORITY.ONE; } }

        abstract override public object Interpret(Context context);
    }
}