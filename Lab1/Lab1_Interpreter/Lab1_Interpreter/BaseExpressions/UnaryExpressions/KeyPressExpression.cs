﻿using System;
using System.Collections.Generic;
using System.Text;
using WindowsInput;
using WindowsInput.Native;

namespace Lab1_Interpreter
{ 
    public class KeyPressExpression : IExpression
    {
        public string keyName { get; private set; }
        public KeyPressExpression(string key)
        {
            keyName = key;
            keyName = keyName.Remove(0, 5);
        }

        public PRIORITY Priority => 0;

        public  object Interpret(Context context)
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
            InputSimulator inputSimulator = new InputSimulator();
            VirtualKeyCode myStatus;
            
            bool res = Enum.TryParse(keyName, out myStatus);
            if (!res)
                return null;
            
            inputSimulator.Keyboard.KeyDown(myStatus);
            inputSimulator.Keyboard.KeyUp(myStatus);
            return null;
        }
    }
}
