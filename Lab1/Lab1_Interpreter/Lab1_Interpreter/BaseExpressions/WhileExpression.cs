﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class WhileExpression : ConditionExpression
    {

        public WhileExpression()
        {
            condition = null;
            posBlockExpression = null;
        }
        public WhileExpression(IExpression condition, IExpression posBlockExpression)
        {
            this.condition = condition;
            this.posBlockExpression = posBlockExpression;
        }
        public override object Interpret(Context context)
        {
            bool cond = Convert.ToBoolean(condition.Interpret(context));
            while (cond)
            {
                posBlockExpression.Interpret(context);
                cond = Convert.ToBoolean(condition.Interpret(context));
            }
            return null;
        }
    }
}
