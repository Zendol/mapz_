﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class NumberExpression:IExpression
    {
        public double value { get; private set; }
        public NumberExpression(double value)
        {
            this.value = value;
        }

        public PRIORITY Priority { get { return PRIORITY.TWO; } }

        public object Interpret(Context context)
        {
            return value;
        }
    }
}
