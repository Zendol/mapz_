﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class BlockExpression : IExpression
    {
        public PRIORITY Priority => PRIORITY.ZERO;
        public List<IExpression> Trees;

        public BlockExpression()
        {
            Trees = new List<IExpression>();
        }
        public BlockExpression(List<IExpression> trees)
        {
            this.Trees = trees;
        }

        
        
        public object Interpret(Context context)
        {
            foreach(var tree in Trees)
            {
               tree.Interpret(context);
                
            }
            return null;
            
        }
    }
}
