﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class RightClickExpression : ClickExpression
    {
        public override object Interpret(Context context)
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightDown);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightUp);
            return null;
        }
    }
}
