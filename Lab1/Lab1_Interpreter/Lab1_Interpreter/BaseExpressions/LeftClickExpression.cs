﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using WindowsInput;
using WindowsInput.Native;

namespace Lab1_Interpreter
{
    public class LeftClickExpression : ClickExpression
    {

        public override object Interpret(Context context)
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
            return null;
        }
    }
}
