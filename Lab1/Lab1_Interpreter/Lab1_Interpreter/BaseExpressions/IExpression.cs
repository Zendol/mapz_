﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public interface  IExpression
    {
        PRIORITY Priority { get;  }
         object Interpret(Context context);
    }

}
