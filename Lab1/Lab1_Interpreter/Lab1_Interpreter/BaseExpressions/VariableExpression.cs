﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class VariableExpression : IExpression
    {
        public string name { get; private set; }
        public double val { get; private set; }
        public PRIORITY Priority { get => PRIORITY.TWO;}

        public VariableExpression(string value)
        {
            this.name = value;
        }

        public  object Interpret(Context context)
        {
            val = context.GetVariable(name);
            return val;
        }
    }
}
