﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public abstract class ConditionExpression : IExpression
    {
        public PRIORITY Priority => PRIORITY.ZERO;

        public IExpression condition;
        public IExpression posBlockExpression;


        public ConditionExpression()
        {
            condition = null;
            posBlockExpression = null;
        }
        public ConditionExpression(IExpression condition, IExpression posBlockExpression)
        {
            this.condition = condition;
            this.posBlockExpression = posBlockExpression;
        }
        public abstract object Interpret(Context context);
    }
}
