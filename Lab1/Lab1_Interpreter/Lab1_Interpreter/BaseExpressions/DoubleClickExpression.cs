﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter.BaseExpressions
{
    public class DoubleClickExpression:ClickExpression
    {
        public override object Interpret(Context context)
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftDown);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
            return null;
        }

    }
}
