﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class IfExpression : ConditionExpression
    {
        public IExpression negBlockExpression;


        public IfExpression()
        {
            condition = null;
            posBlockExpression = null;
            negBlockExpression = null;
        }
        public IfExpression(IExpression condition, IExpression posBlockExpression,IExpression negBlockExpression)
        {
            this.condition = condition;
            this.posBlockExpression = posBlockExpression;
            this.negBlockExpression = negBlockExpression;
        }
        public override object Interpret(Context context)
        {
            bool cond = Convert.ToBoolean(condition.Interpret(context));
            if(cond == true)
            {
                posBlockExpression.Interpret(context);
            }
            else if(negBlockExpression == null)
            {
                return null;
            }
            else
            {
                negBlockExpression.Interpret(context);
            }
            return null;
        }
    }
}
