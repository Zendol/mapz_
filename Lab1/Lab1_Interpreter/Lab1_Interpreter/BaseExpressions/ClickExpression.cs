﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Lab1_Interpreter
{
    public abstract class ClickExpression:IExpression
    {


        public PRIORITY Priority => PRIORITY.ZERO;

        public abstract object Interpret(Context context);
    }
}
