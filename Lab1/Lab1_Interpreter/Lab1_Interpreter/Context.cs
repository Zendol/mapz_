﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class Context
    {
        public Dictionary<string, double> variables { get; private set; }

        public Context()
        {
            variables = new Dictionary<string, double>();
        }

        public double GetVariable(string name)
        {
            if (variables.ContainsKey(name))
                return variables[name];
            else
                return 0;
        }

        public void SetVariable(string name, double value)
        {
            if (variables.ContainsKey(name))
            {
                variables[name] = value;
            }
            else
                variables.Add(name, value);
        }


    }
}
