﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Lab1_Interpreter
{
    public class Parser
    {
        private Stack<IExpression> output;
        private IExpression root;
        public Context context;
        public Parser(Stack<IExpression> output)
        {
            this.output = output;
        }
        
        public IExpression CreateTree()
        {
            if (output.Count > 0)
            {
                root = output.Pop();
                AddToTree(root);
                return root;
            }
            else
                return null;
        }

        private IExpression AddToTree(IExpression node)
        {
            try
            {
                switch (node)
                {
                    case BinaryExpression ex:
                        ex.expr2 = output.Pop();
                        AddToTree(ex.expr2);
                        ex.expr1 = output.Pop();
                        AddToTree(ex.expr1);
                        break;
                    case UnaryExpression ex1:
                        ex1.expr1 = output.Pop();
                        AddToTree(ex1.expr1);
                        break;
                    
                    default:
                        break;
                }
                return node;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }


    }
}
