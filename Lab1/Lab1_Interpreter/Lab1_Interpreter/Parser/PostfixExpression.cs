﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class PostfixExpression
    {

        private Stack<IExpression> stack;
        private List<IExpression> expression;
        public Stack<IExpression> output { get; private set; }

        public PostfixExpression(List<IExpression> ex)
        {
            this.expression = ex;
            stack = new Stack<IExpression>();
            output = new Stack<IExpression>();
        }

        public void CreatePostfixExpression()
        {
            foreach (var ex in expression)
            { 
                if (ex is NumberExpression || ex is VariableExpression)
                    output.Push(ex);

                else if (ex is BinaryExpression)
                {
                    while (stack.Count > 0)
                    {
                        if (!(stack.Peek() is BinaryExpression))
                            break;

                        if (ex.Priority <= stack.Peek().Priority)
                        {
                            output.Push(stack.Pop());
                        }
                        else
                        {
                            break;
                        }
                    }
                    stack.Push(ex);
                }


                else if (ex is LBracketExpression)
                {
                    stack.Push(ex);
                }

                else if (ex is RBracketExpression)
                {
                    IExpression top = null;
                    while (stack.Count > 0 && !((top = stack.Pop()) is LBracketExpression))
                    {
                        output.Push(top);
                    }

                    if (!(top is LBracketExpression))
                        throw new ArgumentException("No mathcing left bracket");
                }

                else
                {
                    stack.Push(ex);
                }
            }
            while (stack.Count > 0)
            {
                var top = stack.Pop();
                if (!expression.Contains(top)) throw new ArgumentException();
                output.Push(top);
            }
        }
    }
}
