﻿using Lab1_Interpreter.BaseExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace Lab1_Interpreter
{
    public class Lexer
    {

        private string input;
        private List<IExpression> tokens;
        List<IExpression> blocks ;
        Stack<IExpression> stack;
        public BlockExpression mainBlock { get; private set; }
        public Context context { get; private set; }
        static public int block { get; private set; } = 0;

        Parser parser;
        IExpression root;
        PostfixExpression postfix;
        Stack<IExpression> output;


        public Lexer(string input)
        {
            this.input = input;
            mainBlock = new BlockExpression();
            tokens = new List<IExpression>();
            blocks = new List<IExpression>();
            stack = new Stack<IExpression>();
        }

        public void Tokenize()
        {
            string[] lines = input.Split('\n');

           context = new Context();
     
            for (int i = 0, n = lines.Length; i < n; i++)
            {
                tokens = AddTokens(lines[i]);
                postfix = new PostfixExpression(tokens);
                postfix.CreatePostfixExpression();
                output = postfix.output;

                //Додавання іф до головного блоку
                if (output.Count > 0 && output.Peek() is IfExpression)
                {
                    AddIfExpression(lines, ref i, n);
                    mainBlock.Trees.Add(root);
                }

                //Додавання While expr до головного блоку
                else if(output.Count > 0 && output.Peek() is WhileExpression)
                {
                    AddWhileExpression(lines, ref i, n);
                    mainBlock.Trees.Add(root);
                }

                else 
                {
                    parser = new Parser(output);
                    parser.context = context;
                    root = parser.CreateTree();
                    Optimizer optimizer = new Optimizer();
                    root = optimizer.OptimizeWithTable(root, context);
                    if (root != null)
                    {
                        if (root is BlockExpression)
                            blocks.Clear();
                        if (block > 0)
                            blocks.Add(root);
                        else
                        {
                            mainBlock.Trees.Add(root);
                            root.Interpret(context);
                        }
                    }
                }
   
            }
            
           // mainBlock.Interpret(context);


        }


        private void AddIfExpression(string[] lines, ref int i, int n)
        {
            IfExpression ifExpression = output.Pop() as IfExpression;
            parser = new Parser(output);
            parser.context = context;
            root = parser.CreateTree();
            ifExpression.condition = root;
            if (i < n - 1 && lines[++i].Contains("{"))
            {
                AddBlock(lines, ref i, n);
                ifExpression.posBlockExpression = root;
            }

            if (i < n - 2 && lines[++i].Contains("else") && lines[++i].Contains("{"))
            {
                AddBlock(lines, ref i, n);
                ifExpression.negBlockExpression = root;
            }

            root = ifExpression;
            root.Interpret(context);
        }

        private void AddWhileExpression(string[] lines, ref int i , int n)
        {
          
            WhileExpression wExpr = output.Pop() as WhileExpression;
            parser = new Parser(output);
            parser.context = context;
            root = parser.CreateTree();
            wExpr.condition = root;
             if (i < n - 1 && lines[++i].Contains("{"))
             {
                 AddBlock(lines, ref i, n);
                 wExpr.posBlockExpression = root;
             }
             root = wExpr;

            root.Interpret(context);
        }

        private List<IExpression> AddTokens(string line)
        {
            tokens = new List<IExpression>();
            List<string> expressions = new List<string>();
            expressions = Regex.Split(line, @"([*+/\- )(])").ToList();
            expressions.RemoveAll(x => x == " " || x == "" || x == "\r");

            foreach (var expression in expressions)
            {
                string ex = expression.Trim('\r', ' ');
                IExpression token = tokenizeExpression(ex);
                tokens.Add(token);
            }
            return tokens;
        }


        private BlockExpression AddBlock(string[] lines, ref int i, int n)
        {
            blocks = new List<IExpression>();
            do
            {
                List<IExpression> tokens = AddTokens(lines[++i]);
                lines[i] = lines[i].Trim('\r', ' ');
                postfix = new PostfixExpression(tokens);
                postfix.CreatePostfixExpression();
                output = postfix.output;

                if (output.Count > 0 && output.Peek() is IfExpression)
                {

                    AddIfExpression(lines, ref i, n);
                    i++;
                }
                else if (output.Count > 0 && output.Peek() is WhileExpression)
                {
                    AddWhileExpression(lines, ref i, n);
                    i++;
                }
                else if (lines[i] == "}")
                {
                    break;
                }
                else
                {
                    parser = new Parser(output);
                    root = parser.CreateTree();
                    Optimizer optimizer = new Optimizer(true);
                    root = optimizer.OptimizeWithTable(root, context);
                }
                if(root!= null)
                    blocks.Add(root);
            } while (i < n - 1);

            List<IExpression> b = blocks.GetRange(0, blocks.Count);
            BlockExpression Block = new BlockExpression(b);
            root = Block;
            return null;
        }

    private IExpression tokenizeExpression(string firstToken)
        {
            IExpression expression = null;


            if (firstToken == "var")
            {
                expression = new VarExpression();
            }

            else if (firstToken == "=")
            {
                expression = new AssignmentExpression();
            }

            else if (firstToken == "||")
            {
                expression = new OrExpression();
            }

            else if (firstToken == "&&")
            {
                expression = new AndExpression();
            }

            else if (firstToken == "==")
            {
                expression = new EqualityExpression();
            }

            else if (firstToken == "!=")
            {
                expression = new NotEqualityExpression();
            }

            else if (firstToken == ">")
            {
                expression = new MoreExpression();
            }

            else if (firstToken == "<")
            {
                expression = new LessExpression();
            }

            else if (firstToken == "+")
            {
                expression = new PlusExpression();
            }
            else if (firstToken == "-")
            {
                expression = new MinusExpression();
            }
            else if (firstToken == "*")
            {
                expression = new MultiplyExpression();
            }
            else if (firstToken == "/")
            {
                expression = new DivisionExpression();
            }

            else if (firstToken == "(")
            {
                expression = new LBracketExpression();
            }

            else if (firstToken == ")")
            {
                expression = new RBracketExpression();
            }

            else if (firstToken == "if")
            {
                expression = new IfExpression();
            }

            else if (firstToken == "while")
            {
                expression = new WhileExpression();
            }
            else if (firstToken == "{")
            {
                block++;
            }
            else if (firstToken == "}")
            {
                block--;
                List<IExpression> b = blocks.GetRange(0, blocks.Count);
                expression = new BlockExpression(b);
            }

            else if (firstToken.All(_ => char.IsDigit(_)))
            {
                expression = new NumberExpression(Convert.ToDouble(firstToken));
            }

            else if(firstToken.Contains("press"))
            {
                expression = new KeyPressExpression(firstToken);
            }
            else if (firstToken == "moveMouse")
            {
                expression = new CursorMoveExpression();
            }

            else if (firstToken == "moveObjects")
            { 
                expression = new ObjectMoveExpression();
            }

            else if (firstToken == "lClick")
            {
                expression = new LeftClickExpression();
            }

            else if (firstToken == "rClick")
            {
                expression = new RightClickExpression();
            }

            else if( firstToken == "dbClick")
            {
                expression = new DoubleClickExpression();
            }
            else if (firstToken.All(_ => char.IsLetterOrDigit(_)))
            {
                expression = new VariableExpression(firstToken);
            }
            else
            {
                throw new Exception($"Unknow expression {firstToken};");
            }

            return expression;
        }

    }
}
