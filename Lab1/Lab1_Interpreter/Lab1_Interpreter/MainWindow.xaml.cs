﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using static Lab1_Interpreter.MouseOperations;

namespace Lab1_Interpreter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {

            InitializeComponent();

  
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string input = Input.Text;
            trvMenu.Items.Clear();
            Output.Text = "";
            Lexer lexer = new Lexer(input);
            lexer.Tokenize();
            BlockExpression mainBlock = lexer.mainBlock;
            MenuItem Root = new MenuItem() { Title = "BlockExpression" };
            for(int i = 0; i < mainBlock.Trees.Count; i++)
            {
                AddTreeItem(Root, mainBlock.Trees[i]);
            }
             
            trvMenu.Items.Add(Root);

            Context context = lexer.context;
            foreach(var v in context.variables)
            {
                Output.Text += $"{v.Key} = {v.Value} \n";
            }
        }


        private  void Tracker()
        {

            MousePoint mousePoint = GetCursorPosition();
             xPos.Text = mousePoint.X.ToString();
             yPos.Text = mousePoint.Y.ToString();

        }



        private void AddTreeItem(MenuItem Root , IExpression expression)
        {
            if (expression is BlockExpression)
            {
                BlockExpression blocks = expression as BlockExpression;
                foreach (var block in blocks.Trees)
                {
                    MenuItem item = new MenuItem() { Title = block.GetType().Name.ToString() };
                    Root.Items.Add(item);
                    AddTreeItem(item, block);
                }
            }

            else if(expression is BinaryExpression)
            {
                BinaryExpression binary = expression as BinaryExpression;
                MenuItem item1 = new MenuItem() { Title = binary.expr1.GetType().Name.ToString() };
                Root.Items.Add(item1);
                AddTreeItem(item1, binary.expr1);
                MenuItem item2 = new MenuItem() { Title = binary.expr2.GetType().Name.ToString() };
                Root.Items.Add(item2);
                AddTreeItem(item2, binary.expr2);
               
            }
            else if (expression is IfExpression)
            {
                IfExpression ifexpr = expression as IfExpression;
                MenuItem ifItem = new MenuItem() { Title = ifexpr.GetType().Name.ToString() };
                AddConditionItem(ifexpr, ifItem);
                if (ifexpr.negBlockExpression != null)
                {
                    MenuItem item3 = new MenuItem() { Title = "(Negative case) " + ifexpr.negBlockExpression.GetType().Name.ToString() };
                    ifItem.Items.Add(item3);
                    AddTreeItem(item3, ifexpr.negBlockExpression);
                }
                Root.Items.Add(ifItem);
            }

            else if (expression is WhileExpression)
            {
                WhileExpression whileExpr = expression as WhileExpression;
                MenuItem whileItem = new MenuItem() { Title = whileExpr.GetType().Name.ToString() };
                AddConditionItem(whileExpr, whileItem);
                Root.Items.Add(whileItem);
            }


            else if(expression is UnaryExpression)
            {
                UnaryExpression unary = expression as UnaryExpression;
                MenuItem item = new MenuItem() { Title = unary.GetType().Name.ToString() };
                MenuItem item1 = new MenuItem() { Title = unary.expr1.GetType().Name.ToString() };
                item.Items.Add(item1);
                AddTreeItem(item1, unary.expr1);
                Root.Items.Add(item);

            }
            else if(expression is NumberExpression)
            {
                NumberExpression number = expression as NumberExpression;
                MenuItem item = new MenuItem() { Title = number.value.ToString() };
                Root.Items.Add(item);
                return;
            }
            else if (expression is VariableExpression)
            {
                VariableExpression number = expression as VariableExpression;
                MenuItem item = new MenuItem() { Title = number.name};
                Root.Items.Add(item);
                return;
            }
            else if(expression is KeyPressExpression)
            {
                KeyPressExpression keyPress = expression as KeyPressExpression;
                MenuItem item = new MenuItem() { Title = expression.GetType().Name + " ( " +keyPress.keyName + " )"};
                Root.Items.Add(item);
                return;
            }
        }

        private void AddConditionItem(IExpression expression, MenuItem Root)
        {
            ConditionExpression condExpr = expression as ConditionExpression;
            MenuItem item = new MenuItem() { Title = "(Condition) " + condExpr.condition.GetType().Name.ToString() };
            Root.Items.Add(item);
            AddTreeItem(item, condExpr.condition);
            MenuItem item2 = new MenuItem() { Title = "(Positive case) " + condExpr.posBlockExpression.GetType().Name.ToString() };
            Root.Items.Add(item2);
            AddTreeItem(item2, condExpr.posBlockExpression);
        }

        private void Input_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Key == Key.Enter)
            {
                Button_Click(this, new RoutedEventArgs());
            }
        }



        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            Tracker();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Output.AppendText("Lol\n");
        }
    }
}
