﻿using DialogGame_HarryPotterSimulation.Characters;
using DialogGame_HarryPotterSimulation.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class SpellLearnViewModel:BaseViewModel
    {
        private PointCollection points = new PointCollection();
        private PlayerCharacter playerCharacter;
        private Spell spell;
        private RelayCommand addSpellCommand;

        public Spell Spell
        {
            get { return spell; }
            set
            {
                spell = value;
                OnPropertyChanged("Spell");
            }
        }



        public SpellLearnViewModel(PlayerCharacter playerCharacter)
        {
            this.playerCharacter = playerCharacter;
            Spell = SpellPatterns.GetSpell(playerCharacter.Level);
            if (playerCharacter.LearnedSpells.Count != 0 && playerCharacter.LearnedSpells.Count - 1 == playerCharacter.Level)
                return;
            else
                playerCharacter.LearnedSpells.Add(Spell);
        }


        
      
        public PointCollection Points
        {
            get { return points; }
            set { points = value;
                OnPropertyChanged("Points");
            }
        }

    }
}
