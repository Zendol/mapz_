﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace DialogGame_HarryPotterSimulation.Characters
{
    public class Characteristic:INotifyPropertyChanged
    {
        private string characteristicName;
        private string characteristicImg;
        private int points;

        public string CharacteristicName
        {
            get { return characteristicName; }
            set
            {
                characteristicName = value;
                OnPropertyChanged("CharacteristicName");
            }
        }

        public string CharacteristicImg
        {
            get { return characteristicImg; }
            set
            {
                characteristicImg = value;
                OnPropertyChanged("CharacteristicImg");
            }
        }

        public int Points
        {
            get { return points; }
            set
            {
                points = value;
                OnPropertyChanged("Points");
            }
        }

        public Characteristic() { }

        public Characteristic(string name)
        {
            characteristicName = name;
            points = 0;
        }

        public void IncrementPoints()
        {
            points++;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
