﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.DialogWork.DialogBuild
{
    public class OptionDialogLine : DialogLine
    {
        public OptionBox OptionBox { get; private set; }

        public OptionDialogLine(string Name, string Content, OptionBox optionBox):base(Name,Content)
        {
            OptionBox = optionBox;
        }
                    
    }
}
