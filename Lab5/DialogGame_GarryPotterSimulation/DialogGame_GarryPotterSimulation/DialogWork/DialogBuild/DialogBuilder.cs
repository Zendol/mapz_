﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.DialogWork.DialogBuild
{
    abstract class DialogBuilder
    {
        public Dialog Dialog { get; protected set; }
        public abstract void SetLocation(string loc);
        public abstract void SetLocationImg();
        public abstract void SetDialogLines(List<DialogLine> lines);

    }
}
