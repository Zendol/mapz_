﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace DialogGame_HarryPotterSimulation
{
    public abstract class DialogComponent
    {
        public DialogComponent() { }
        public virtual void AddLine(DialogComponent component) { }
    }
    public class DialogLine: DialogComponent
    {
        public int LineId;
        public string Name { get; private set; }
        public string Content { get; private set; }
        public string CharacterImg { get;  set; }


        public DialogLine(string Name, string Content)
        {
            this.Name = Name;
            this.Content = Content;
        }

    }
}
