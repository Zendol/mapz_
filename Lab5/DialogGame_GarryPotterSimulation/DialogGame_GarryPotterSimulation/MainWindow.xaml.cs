﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialogGame_HarryPotterSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void partsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void menuBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        private void newGameBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWorld gameWindow = new GameWorld(false);
            gameWindow.Show();
            this.Close();
        }

        private void loadGameBtn_Click(object sender, RoutedEventArgs e)
        {
            GameWorld gameWindow = new GameWorld(true);
            gameWindow.Show();
            this.Close();
        }
    }
}
