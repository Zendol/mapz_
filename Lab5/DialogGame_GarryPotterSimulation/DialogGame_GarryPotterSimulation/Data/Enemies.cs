﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.Data
{
    static class Enemies
    {
        static public readonly Dictionary<int, EnemyCharacter> EnemiesCharacter = new Dictionary<int, EnemyCharacter>()
        {
            {0, new EnemyCharacter("Drako","../Images/characters/Hermione.PNG", 
                new List<Spell>(){new Spell(), new Spell(), new Spell()})}
        };
    }
}
