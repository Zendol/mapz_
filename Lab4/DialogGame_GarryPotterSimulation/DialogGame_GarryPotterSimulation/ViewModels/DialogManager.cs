﻿using DialogGame_HarryPotterSimulation;
using DialogGame_HarryPotterSimulation.Characters;
using DialogGame_HarryPotterSimulation.DialogWork.DialogBuild;
using DialogGame_HarryPotterSimulation.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class DialogManager: BaseViewModel, INotifyPropertyChanged
    {
        private DialogParser parser;

        private PlayerCharacter playerCharacter;

        private Dialog dialog;

        private DispatcherTimer Timer;

        private CareTaker careTaker;

        private string time;

        private string text, characterName,location;

        private string characterImg, locationImg;

        private static int currentLineNum;

        private static int currentDialog;

        private int dialogNum;

        private ObservableCollection<string> options;

        private int selectedIndex;

        private Option selectedOption;

       

        private static readonly Lazy<DialogManager> lazy =  new Lazy<DialogManager>(() => new DialogManager());

        private RelayCommand updateCommand;

        public event PropertyChangedEventHandler PropertyChanged;
      
        private DialogManager()
        {
            parser = DialogParser.GetInstance();
            Options = new ObservableCollection<string>();
            currentLineNum = 0;
        }
        public static DialogManager GetInstance()
        {
            return lazy.Value;
        }

        public void Start (int dialogCount, PlayerCharacter playerCharacter)
        {
            dialogNum = dialogCount;
            this.playerCharacter = playerCharacter;
            currentDialog = playerCharacter.Level;
            parser.Start(currentDialog);
            dialog = parser.CreateDialog();
            Location = dialog.Location;
            LocationImg = dialog.LocationImg;
            Update();
        }


        public RelayCommand UpdateCommand
        {
            get
            {
                return updateCommand ??
                  (updateCommand = new RelayCommand(obj =>
                  {
                      Update();
                  }));
            }
        }

        public string CharacterName
        {
            get { return characterName; }
            set
            {
                characterName = value;
                OnPropertyChanged("CharacterName");
            }
        }

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }

        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                OnPropertyChanged("Location");
            }
        }

        public string LocationImg
        {
            get { return locationImg; }
            set
            {
                locationImg = value;
                OnPropertyChanged("LocationImg");
            }
        }

        public string CharacterImg
        {
            get { return characterImg; }
            set
            {
                characterImg = value;
                OnPropertyChanged("CharacterImg");
            }
        }

        public ObservableCollection<string> Options
        {
            get { return options; }
            set
            {
                options = value;
                OnPropertyChanged("Options");
            }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                CheckOption();
                //UpdateCommand.Execute(this);
                OnPropertyChanged("SelectedIndex");
            }
        }


        public string Time
        {
            get { return time; }
            set
            {
                time = value;
                OnPropertyChanged("Time");
            }
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        void Update()
        {
            if(dialogNum == currentDialog)
            {
                return;
            }
            if (currentLineNum == dialog.DialogLines.Count)
            {
                NewLevel();
                Update();
                return;
            }
            ShowDialog();
            currentLineNum++;
            playerCharacter.Stage = currentLineNum/(dialog.DialogLines.Count * 1.0) * 100;
        }

        public void NewLevel()
        {
            currentDialog++;
            parser.Start(currentDialog);
            dialog = parser.CreateDialog();
            Location = dialog.Location;
            LocationImg = dialog.LocationImg;
            currentLineNum = 0;
            playerCharacter.Money += 10;
            playerCharacter.Level = currentDialog;
            careTaker = new CareTaker();
            careTaker.LevelMarker = playerCharacter.SaveGame();
            MessageBox.Show($"Woooooo new level {playerCharacter.Level}\n" +
                $"You've got +10 coins");
        }

        public void ShowDialog()
        {
            ParseLine();
        }


        public void CheckOption()
        {
            if (selectedIndex == -1)
                return;
            else if(currentLineNum-1 < dialog.DialogLines.Count
                && dialog.DialogLines[currentLineNum-1] is OptionDialogLine option){
                selectedOption  =  option.OptionBox.Options[selectedIndex];
                if(selectedOption is CharacteristicOption)
                {
                    playerCharacter.AddCharacteristicPoint(selectedIndex);
                    MessageBox.Show($"You've got + 1 point to\n " +
                        $"{playerCharacter.Characteristics[selectedIndex].CharacteristicName}");
                }
                else if(selectedOption is SpecialOffer specialOffer)
                {
                    if(playerCharacter.Money < specialOffer.Cost)
                    {
                        MessageBox.Show("You don't have enough money");
                    }
                    else
                    {
                        playerCharacter.Money -= specialOffer.Cost;
                        for (int i = 0; i < playerCharacter.Characteristics.Count; i++)
                        {
                            playerCharacter.AddCharacteristicPoint(i);
                        }
                        MessageBox.Show($"You've got + 1 point to\n all attributes");
                    }
                    
                }

            }
        }

        void ParseLine()
        {
            if (currentLineNum < dialog.DialogLines.Count)
            {
                if (dialog.DialogLines[currentLineNum] is OptionDialogLine optionDialogLine
               && optionDialogLine.OptionBox != null)
                {
                    Options = new ObservableCollection<string>();
                    switch (optionDialogLine.OptionBox)
                    {
                        case TimeOptionBox timeOption:
                            {
                                Time = timeOption.Seconds.ToString();
                                StartTimer();
                                break;
                            }
                        case OptionBox optionBox:
                            {
                                StopTimer();
                                break;
                            }
                    }
                    if (optionDialogLine.OptionBox.Options[0] is SpecialOffer specialOffer)
                    {
                        Options.Add($"{specialOffer.Text}   ( {specialOffer.Cost} coins )");
                        for (int i = 1; i < optionDialogLine.OptionBox.Options.Count; i++)
                            Options.Add(optionDialogLine.OptionBox.Options[i].Text);
                    }
                    else
                    {
                        optionDialogLine.OptionBox.Options.ForEach(x => Options.Add(x.Text));
                    }          
                }
                else
                {
                    Options = null;
                    StopTimer();
                }
                CharacterName = dialog.DialogLines[currentLineNum].Name;
                CharacterImg = dialog.DialogLines[currentLineNum].CharacterImg;
                Text = dialog.DialogLines[currentLineNum].Content;
            }
        }

        private void StartTimer()
        {
            Timer = new DispatcherTimer();
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Tick += Timer_Tick;
            Timer.Start();
        }

        private void StopTimer()
        {
            if(Timer != null)
            {
                Timer.Tick -= Timer_Tick;
                Timer.Stop();
                Time = "";
            }
            
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            int curTime = Convert.ToInt32(Time);
            if(curTime > 0)
            {
                curTime--;
                Time = curTime.ToString() ;
            }
            else
            {
                StopTimer();
            }
        }

    }
}
