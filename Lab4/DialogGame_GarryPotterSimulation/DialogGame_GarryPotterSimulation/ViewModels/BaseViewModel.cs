﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class BaseViewModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string properyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(properyName));
        }
    }
}
