﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace DialogGame_HarryPotterSimulation.Data
{
    static public class SpellPatterns
    {
        private static readonly List<Spell> spells;
        static SpellPatterns()
        {
            spells = new List<Spell>();
            spells.Add(spell1);
        }
        private static readonly PointCollection figure1 = new PointCollection()
        {
            new Point(210,400),
            new Point(325,308),
            new Point(345,328),
            new Point(385,270),
            new Point(335,200),
            new Point(315,127),
            new Point(360,77),
            new Point(310,36),
            new Point(215,88),
            new Point(125,36),
            new Point(75,77),
            new Point(120,127),
            new Point(100,200),
            new Point(50,270),
            new Point(95,328),
            new Point(115,308)
        };

        private static readonly Spell spell1 = new Spell("Anapneo",SpellType.Heal, "Clears the target's airway if they are choking on something."
            , "ah-NAP-nee-oh", figure1);
        public static Spell GetSpell(int level)
        {
            if(spells.Count - 1>= level)
            {
                return spells[level];
            }
            return null;
        }
    }
}
