﻿using DialogGame_HarryPotterSimulation.Characters;
using DialogGame_HarryPotterSimulation.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace DialogGame_HarryPotterSimulation
{
    class UpdateViewCommand:ICommand
    {
        private GameWorldViewModel viewModel;
        private PlayerCharacter playerCharacter;

        public UpdateViewCommand(GameWorldViewModel viewModel, PlayerCharacter playerCharacter)
        {
            this.playerCharacter = playerCharacter;   
            this.viewModel = viewModel;
            Execute("Dialog");
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parametr)
        {
            return true;
        }

        public void Execute(object parametr)
        {
            if(parametr.ToString() == "Profile")
            {
                viewModel.SelectedViewModel = new PlayerProfile(playerCharacter);
            }
            else if(parametr.ToString() == "Spell")
            {
                viewModel.SelectedViewModel = new SpellLearnViewModel(playerCharacter);
            }
            else if(parametr.ToString() == "Duel")
            {
                viewModel.SelectedViewModel = new DuelViewModel(playerCharacter);
            }
            else if(parametr.ToString() == "Dialog")
            {
                DialogManager dialogManager = DialogManager.GetInstance();
                viewModel.SelectedViewModel = dialogManager;
                dialogManager.Start(3, playerCharacter);
            }
        }
    }
}
