﻿using DialogGame_HarryPotterSimulation.Data;
using DialogGame_HarryPotterSimulation.DialogWork.DialogBuild;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace DialogGame_HarryPotterSimulation
{
    public class DialogParser
    {

        private static readonly DialogParser instance = new DialogParser();
        public static DialogParser GetInstance()
        {
            return instance;
        }

        public int lineCount { get; private set; }
        private string location;
        private DialogBuilder dialogBuilder = new OrdinaryDialogBuilder();
        private List<DialogLine> lines;

        public void Start(int sceneNum)
        {
            string file = Environment.CurrentDirectory + "\\Dialogs\\Dialog";
            file += sceneNum + 1;
            file += ".txt";
            lineCount = 0;
            lines = new List<DialogLine>();
            LoadDialog(file);
        }


   


        public void LoadDialog(string filename)
        {
            string line;
            if (!File.Exists(filename))
                return;
            StreamReader r = new StreamReader(filename);

            using (r)
            {
                line = r.ReadLine();
                location = line;
              
                do
                {
                    line = r.ReadLine();
                    if (line != null && line.Length > 1)
                    {
                        string[] lineData = line.Split(';');

                        if (lineData[0] == "OrdOption")
                        {
                            AddOrdOption(lineData);
                        }
                        else if (lineData[0] == "SpecialOrdOption")
                        {
                            AddSpecialOffer(lineData);
                        }

                        else if (lineData[0] == "UnOrdOption")
                        {
                            AddUnOrdOption(lineData);
                        }
                        else
                        {
                            DialogLine lineEntry = new DialogLine(lineData[0], lineData[1]);
                            lines.Add(lineEntry);
                        }
                        lines[lineCount].CharacterImg = DialogImage.characterImgs[lines[lineCount].Name];
                    }

                    lineCount++;

                }
                while (line != null);
                r.Close();
            }
        }


        public void AddOrdOption(string[] lineData)
        {
            Random rand = new Random();
            TimeOptionBox optionBox = new TimeOptionBox(rand.Next(15, 30));
            OptionDialogLine lineEntry = new OptionDialogLine(lineData[1], lineData[2], optionBox);
            for (int i = 3; i < lineData.Length; i++)
            {
                lineEntry.OptionBox.Options.Add(new Ordynary(lineData[i]));
            }
            lines.Add(lineEntry);
        }

        public void AddUnOrdOption(string[] lineData)
        {
            OptionBox optionBox = new OptionBox();
            OptionDialogLine lineEntry = new OptionDialogLine(lineData[1], lineData[2], optionBox);
            var types = Enum.GetValues(typeof(OptionType)).Cast<OptionType>().ToList();

            for (int i = 3; i < lineData.Length; i++)
            {
                if(lineData[i] != "")
                    lineEntry.OptionBox.Options.Add(new CharacteristicOption(types[i - 3], new Ordynary(lineData[i])));
            }
           // lineEntry.OptionBox.Options.Shuffle();
            lines.Add(lineEntry);
        }

        public void AddSpecialOffer(string[] lineData)
        {
            OptionBox optionBox = new OptionBox();
            OptionDialogLine lineEntry = new OptionDialogLine(lineData[1], lineData[2], optionBox);
            Random rand = new Random();
            lineEntry.OptionBox.Options.Add(new SpecialOffer(rand.Next(10, 20), new Ordynary(lineData[3])));
            for (int i = 4; i < lineData.Length; i++)
            {
                lineEntry.OptionBox.Options.Add(new Ordynary(lineData[i]));
            }
            lines.Add(lineEntry);
        } 

        public Dialog CreateDialog()
        {
            dialogBuilder.SetDialogLines(lines);
            dialogBuilder.SetLocation(location);
            dialogBuilder.SetLocationImg();
            return dialogBuilder.Dialog;
        }



    }

    public static class ThreadSafeRandom
    {
        [ThreadStatic] private static Random Local;

        public static Random ThisThreadsRandom
        {
            get { return Local ?? (Local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
        }
    }

    static class MyExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}

