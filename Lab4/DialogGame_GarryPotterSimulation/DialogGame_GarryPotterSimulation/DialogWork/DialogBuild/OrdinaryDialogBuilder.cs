﻿using DialogGame_HarryPotterSimulation.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.DialogWork.DialogBuild
{
    class OrdinaryDialogBuilder : DialogBuilder
    {
        public OrdinaryDialogBuilder()
        {
            this.Dialog = new Dialog();
        }
        public override void SetDialogLines(List<DialogLine> lines)
        {
            this.Dialog.DialogLines = lines;
        }

        public override void SetLocation(string loc)
        {
            this.Dialog.Location = loc;
        }

        public override void SetLocationImg()
        {
            this.Dialog.LocationImg = DialogImage.locationImgs[Dialog.Location];
        }


    }

}
