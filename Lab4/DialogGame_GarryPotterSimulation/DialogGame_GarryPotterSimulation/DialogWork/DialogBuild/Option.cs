﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace DialogGame_HarryPotterSimulation
{
    abstract public class Option 
    {
        public int DestinationId { get; private set; } = -1;
        protected string text;

        public string Text
        {
            get { return text; } 
            set{ text = value; } 
        }

        public Option()
        {
            text = "";
        }
        public Option(string Text)
        {
            text = Text;
        }
    }

    abstract public class OptionDecorator:Option
    {
        protected Option option;
        public OptionDecorator(string n, Option option) : base(n)
        {
            this.option = option;
        }
    }
    public class Ordynary:Option
    {   
        public Ordynary(string text): base(text)
        {
        }
    }
    public class SpecialOffer:OptionDecorator
    {
        public int Cost { get; private set; }
        public SpecialOffer(int Cost, Option option):base(option.Text , option)
        {
            this.Cost = Cost;
        }
    }

    public class CharacteristicOption:OptionDecorator
    {
        protected OptionType Type { get; set; }
        public CharacteristicOption(OptionType Type, Option option):base(option.Text,option)
        {
            this.Type = Type;
        }
    }

    public class OptionBox
    {
        public List<Option> Options { get; private set; }
        public OptionBox()
        {
            this.Options = new List<Option>();
        }
    }

    public class TimeOptionBox:OptionBox
    {
        private int seconds;

        public int Seconds { get { return seconds; } private set { seconds = value; } }
        public TimeOptionBox(int sec)
        {
            seconds = sec;
        }
    }


   



    public enum OptionType
    {
        Gryffindor,
        Ravenclaw,
        Hufflepuff,
        Slytherin
    }

}
