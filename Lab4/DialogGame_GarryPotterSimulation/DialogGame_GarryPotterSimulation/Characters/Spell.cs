﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;

namespace DialogGame_HarryPotterSimulation.Characters
{
    public class Spell : INotifyPropertyChanged
    {
        private string name;
        private SpellType type;
        private string description;
        private string pronuciation;
        private PointCollection pattern;


        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public SpellType Type
        {
            get { return type; }
            set
            {
                type = value;
                OnPropertyChanged("Type");
            }
        }



        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }
        public string Pronuciation
        {
            get { return pronuciation; }
            set
            {
                pronuciation = value;
                OnPropertyChanged("Pronuciation");
            }
        }

        public PointCollection Pattern
        {
            get { return pattern; }
            set
            {
                pattern = value;
                OnPropertyChanged("Pattern");
            }
        }
        public Spell(){}
        
        public Spell(string name, string description, SpellType type)
        {
            this.name = name;
            this.description = description;
            this.type = type;
        }
        public Spell(string name, SpellType type, string description, string pronuciation, PointCollection pattern)
        {
            this.name = name;
            this.type = type;
            this.description = description;
            this.pronuciation = pronuciation;
            this.pattern = pattern;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string properyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(properyName));
        }
    }

    public interface IVisitor
    {
        void VisitPlayerCharacter(PlayerCharacter playerCharacter);
        void VisitEnemyCharacter(EnemyCharacter enemyCharacter);
    }



    public enum SpellType
    {
        Heal,
        Attack,
        Stun
    }

    public interface ISpellable
    {
        void UseSpell(PlayerCharacter playerCharacter, EnemyCharacter enemyCharacter);
    }

    public class HealUse:ISpellable
    {
        private int healPoints = 5;

        public void UseSpell(PlayerCharacter playerCharacter, EnemyCharacter enemyCharacter)
        {
            Random rnd = new Random();
            if (playerCharacter.HP > 85)
                playerCharacter.HP = 100;
            else
            {
                int actualHeal = healPoints + rnd.Next(5, 10);
                playerCharacter.HP += actualHeal;
            }
           
        }
    }

    public class AttackUse:ISpellable
    {
        private int damage = 15;
        public void UseSpell(PlayerCharacter playerCharacter, EnemyCharacter enemyCharacter)
        {
            Random rnd = new Random();
            int actDamage = damage - rnd.Next(5, 15);
            enemyCharacter.HP -= actDamage;
            playerCharacter.Send($"Enemy have got -{actDamage}\n");
        }
    }

    public class StunUse:ISpellable
    {
        private int damage = 5;
        public void UseSpell(PlayerCharacter playerCharacter, EnemyCharacter enemyCharacter)
        {
            enemyCharacter.HP -= damage;
        }
    }

}
