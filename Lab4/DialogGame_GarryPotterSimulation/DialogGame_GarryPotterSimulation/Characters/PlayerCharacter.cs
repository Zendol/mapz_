﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Xml.Serialization;

namespace DialogGame_HarryPotterSimulation.Characters
{
    public class PlayerCharacter:Character,INotifyPropertyChanged
    {


        private readonly string gameSavePath = "gamesave.xml";

        public List<Characteristic> Characteristics { get; set; } = new List<Characteristic>()
        {
            new Characteristic("Courage"),
            new Characteristic("Honesty"),
            new Characteristic("Wisdom"),
            new Characteristic("Cunning")
        };

        private int money = 30;

        public string DuelLog
        {
            get { return duelLog; }
            set {  duelLog = value;}
        }
        public int Money
        {
            get { return money; }
            set
            {
                money = value;
                OnPropertyChanged("Money");
            }
        }

        private double healthPoints = 100;

        public double HP
        {
            get { return healthPoints; }
            set
            {
                healthPoints = value;
                OnPropertyChanged("HP");
            }
        }

        private int level = 0;
        public int Level
        {
            get { return level; }
            set
            {
                level = value;
                OnPropertyChanged("Level");
            }
        }

        private double stage = 0;
        public double Stage
        {
            get { return stage; }
            set
            {
                stage = value;
                OnPropertyChanged("Stage");
            }
        }

        private ObservableCollection<Spell> learnedSpells;
        public ObservableCollection<Spell> LearnedSpells
        {
            get { return learnedSpells; }
            set
            {
                learnedSpells = value;
                OnPropertyChanged("LearnedSpells");
            }
        }



        public PlayerCharacter()
        {
            money = 30;
            learnedSpells = new ObservableCollection<Spell>();
            learnedSpells.Add(new Spell("Flipendo", "Knocks objects and creatures backwards",SpellType.Attack));
            learnedSpells.Add(new Spell("Silencio", "Makes something silent",SpellType.Stun));
        }

        public PlayerCharacter(Mediator mediator)
        {
            this.mediator = mediator;
        }

        public void AddCharacteristicPoint(int selectedChar)
        {
            if (selectedChar > Characteristics.Count)
                return;
            Characteristics[selectedChar].IncrementPoints();
        }

        

        public PlayerMemento SaveGame()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(PlayerMemento));
            using FileStream fs = new FileStream(gameSavePath, FileMode.OpenOrCreate);
            PlayerMemento memento = new PlayerMemento(stage, level, money, Characteristics, LearnedSpells);
            formatter.Serialize(fs, memento);
            return memento;
        }

        public void RestoreGame()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(PlayerMemento));
            using FileStream fs = new FileStream(gameSavePath, FileMode.OpenOrCreate);
            PlayerMemento memento = (PlayerMemento)formatter.Deserialize(fs);
            stage = memento.Stage;
            level = memento.Level;
            money = memento.Money;
            Characteristics = memento.Characteristics;
            LearnedSpells = memento.Spells;
        }

        public ISpellable Spellable { private get; set; }
        public void UseSpell(ISpellable spellable, EnemyCharacter enemyCharacter)
        {
            spellable.UseSpell(this, enemyCharacter);
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string properyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(properyName));
        }

        public override void Notify(string message)
        {
            DuelLog += message;
        }
    }
}
