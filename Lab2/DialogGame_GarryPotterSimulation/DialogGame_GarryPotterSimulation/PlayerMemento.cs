﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DialogGame_HarryPotterSimulation
{
    [Serializable]
    public class PlayerMemento
    {
        public double Stage { get;  set; }
        public int Level { get;  set; }
        public int Money { get; set; }
        public List<Characteristic> Characteristics { get;  set; }
        public ObservableCollection<Spell> Spells { get;  set; }

        public PlayerMemento() { }
        public PlayerMemento(double stage, int level, int money, List<Characteristic> characteristics, ObservableCollection<Spell> spells)
        {
            Stage = stage;
            Level = level;
            Money = money;
            Characteristics = characteristics;
            Spells = spells;
        }
    }

    public class CareTaker
    {
        public PlayerMemento LevelMarker;
    }
}
