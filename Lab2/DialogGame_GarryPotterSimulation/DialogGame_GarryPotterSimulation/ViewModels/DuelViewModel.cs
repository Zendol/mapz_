﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class DuelViewModel:BaseViewModel
    {
        private PlayerCharacter playerCharacter;
        private EnemyCharacter enemyCharacter;

        private DuelMediator DuelMediator;
        private RelayCommand useSpellCommand;
        private Spell selectedSpell;
        public EnemyCharacter EnemyCharacter
        {
            get { return enemyCharacter; }
            set
            {
                enemyCharacter = value;
                OnPropertyChanged("EnemyCharacter");
            }
        }

        public PlayerCharacter PlayerCharacter
        {
            get { return playerCharacter; }
            set
            {
                playerCharacter = value;
                OnPropertyChanged("PlayerCharacter");
            }
        }

        public Spell SelectedSpell
        {
            get { return selectedSpell; }
            set
            {
                selectedSpell = value;
                UpdateDuel();
                OnPropertyChanged("SelectedSpell");
            }
        }

        public string duelLog;
        public string DuelLog
        {
            get { return duelLog; }
            set
            {
                duelLog = value;
                OnPropertyChanged("DuelLog");
            }
        }
        public RelayCommand UseSpellCommand
        {
            get
            {
                return useSpellCommand ??
                  (useSpellCommand = new RelayCommand(obj =>
                  {
                     // Update();
                  }));
            }
        }

        public void StartDuel()
        {
            
        }

        public void UpdateDuel()
        {
            if(SelectedSpell.Type == SpellType.Attack)
            {
                playerCharacter.UseSpell(new AttackUse(), enemyCharacter);
                if(enemyCharacter.HP < 0)
                {
                    MessageBox.Show("You've won");
                    Spells = null;
                }
                EnemyTurn();
            }
            else if(SelectedSpell.Type == SpellType.Heal)
            {
                playerCharacter.UseSpell(new HealUse(), enemyCharacter);
                EnemyTurn();
            }

            else if(SelectedSpell.Type == SpellType.Stun)
            {
                playerCharacter.UseSpell(new StunUse(), enemyCharacter);
            }
            DuelLog = playerCharacter.DuelLog;

        }

        public void EnemyTurn()
        {
            Array values = Enum.GetValues(typeof(SpellType));
            Random random = new Random();
            SpellType randomSpell = (SpellType)values.GetValue(random.Next(values.Length));
            if (randomSpell == SpellType.Attack)
            {
                
                Random rnd = new Random();
                int damage = rnd.Next(15, 25);
                playerCharacter.HP -= damage;
                if (playerCharacter.HP < 0)
                {
                    MessageBox.Show("You've lost");
                    Spells = null;
                }
                enemyCharacter.Send($"You've got -{damage} hp");
            }
            else if (randomSpell == SpellType.Heal)
            {
                Random rnd = new Random();
                if (enemyCharacter.HP > 85)
                    enemyCharacter.HP = 100;
                else
                    enemyCharacter.HP += rnd.Next(5, 15);
            }

            else if(randomSpell == SpellType.Stun)
            {
                playerCharacter.HP -= 5;
                enemyCharacter.Send("You've got -5 hp");
            }
        }
        public ObservableCollection<Spell> Spells { get; set; }
        public DuelViewModel(PlayerCharacter playerCharacter)
        {
            DuelMediator = new DuelMediator();
            this.playerCharacter = playerCharacter;
            enemyCharacter = new EnemyCharacter(playerCharacter.Level,DuelMediator);
            this.playerCharacter.Mediator = DuelMediator;
            Spells = playerCharacter.LearnedSpells;
            DuelMediator.PlayerCharacter = this.playerCharacter;
            DuelMediator.EnemyCharacter = enemyCharacter;
            this.playerCharacter.DuelLog = "";
            this.playerCharacter.HP = 100;
        }
    }

}
