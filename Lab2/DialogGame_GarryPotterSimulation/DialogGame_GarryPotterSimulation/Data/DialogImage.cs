﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.Data
{
    static class DialogImage
    {
        static readonly string rootCharactImg = "Images/characters/";
        static string rootLocationImg = "Images/locations/";
        static public readonly Dictionary<string, string> locationImgs = new Dictionary<string, string>()
        {
            {"Library", rootLocationImg + "Library.jpg"},
            {"Platform",rootLocationImg + "Platform.png"},
            {"Train", rootLocationImg+"Train.jpg" },
            {"Hall", rootLocationImg +"Hall.png" }
        };

        static public readonly Dictionary<string, string> characterImgs = new Dictionary<string, string>()
        {
            {"Harry", rootCharactImg + "Harry.PNG"},
            {"Ron",rootCharactImg + "Ron.PNG"},
            {"Hermione",rootCharactImg + "Hermione.PNG"},
            {"McGonagall", rootCharactImg+"McGonagall.PNG" }
        };
    }
}
