﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogGame_HarryPotterSimulation.DialogWork.DialogBuild
{

    public class Dialog:DialogComponent
    {
        public List<DialogLine> DialogLines { get; set; }
        public string Location { get;  set; }
        public string LocationImg { get;  set; }
        public void AddLine(DialogLine line)
        {
            if (line == null)
                return;
            DialogLines.Add(line);

            line.LineId = DialogLines.IndexOf(line);   
        }
    }
}
