﻿using DialogGame_HarryPotterSimulation.ViewModels;
using DialogGame_HarryPotterSimulation.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DialogGame_HarryPotterSimulation
{
    /// <summary>
    /// Логика взаимодействия для GameWorld.xaml
    /// </summary>
    public partial class GameWorld : Window
    {
        public GameWorld(bool isLoaded)
        {
            InitializeComponent();
            DataContext = new GameWorldViewModel(isLoaded);
        }

    }
}
