﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DialogGame_HarryPotterSimulation.Characters
{
    abstract public class Character
    {
        protected string name;
        protected string house;
        protected List<Spell> learnedSpells;
        protected string duelLog;

        protected Mediator mediator;

        public Mediator Mediator
        {
            get { return mediator; }
            set { mediator = value; }
        }
        public virtual void Send(string message)
        {
            mediator.Send(message, this);
        }
        public abstract void Notify(string message);
    }

    public abstract class Mediator
    {
        public abstract void Send(string msg, Character character);
    }

    public class DuelMediator:Mediator
    {
        public PlayerCharacter PlayerCharacter;
        public EnemyCharacter EnemyCharacter;

        public override void Send(string msg, Character character)
        {
            if(PlayerCharacter == character)
            {
                PlayerCharacter.Notify(msg);
            }
            else if(EnemyCharacter == character)
            {
                EnemyCharacter.Notify(msg);
            }
        }
    }
}
