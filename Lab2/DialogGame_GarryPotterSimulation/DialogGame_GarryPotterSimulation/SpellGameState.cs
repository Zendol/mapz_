﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace DialogGame_HarryPotterSimulation
{

    public class SpellGame
    {

        public ISpellGameState State { get; set; }

        public SpellGame(ISpellGameState spellGameState)
        {
            State = spellGameState;
        }

        public void Start()
        {
            State.Start(this);
        }

        public void Finish()
        {
            State.Finish(this);
        }

        public void UnStart()
        {
            State.UnStart(this);
        }
    }
    public interface ISpellGameState
    {
        void UnStart(SpellGame spellGame);
        void Start(SpellGame spellGame);
        void Finish(SpellGame spellGame);
    }

    public class EndedSpellGameState : ISpellGameState
    {
        public void Finish(SpellGame spellGame)
        {
            spellGame.State = new EndedSpellGameState();
        }

        public void Start(SpellGame spellGame)
        {
            spellGame.State = new StartedSpellGameState();
        }

        public void UnStart(SpellGame spellGame)
        {
            spellGame.State = new UnStartedSpellGame();
        }
    }

    public class StartedSpellGameState : ISpellGameState
    {
        public RelayCommand AddSpellCommand { get; set; }
        public void Finish(SpellGame spellGame)
        {
            spellGame.State = new EndedSpellGameState();
            MessageBox.Show("Niceeeeeeee,\n you've learnt a new spell");
        }

        public void Start(SpellGame spellGame)
        {
            spellGame.State = new StartedSpellGameState();
        }

        public void UnStart(SpellGame spellGame)
        {
            spellGame.State = new UnStartedSpellGame();
            MessageBox.Show("Unfortunately you lost,\n try to start again");
        }
    }

    public class UnStartedSpellGame : ISpellGameState
    {
        public void Finish(SpellGame spellGame)
        {
            MessageBox.Show("You should move to start, before ending");
        }

        public void Start(SpellGame spellGame)
        {
            spellGame.State = new StartedSpellGameState();
        }

        public void UnStart(SpellGame spellGame)
        {
            spellGame.State = new UnStartedSpellGame();
        }
    }


}
