﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class PlayerProfile:BaseViewModel,INotifyPropertyChanged
    {
        public ObservableCollection<Characteristic> Characteristics { get; set; }
        public ObservableCollection<Spell> Spells { get; set; }
        public PlayerProfile(PlayerCharacter playerCharacter)
        {
            Characteristics = new ObservableCollection<Characteristic>
            {
                new Characteristic {CharacteristicImg = "pack://application:,,,/Images/characteristic/shield.png", CharacteristicName = "Courage", Points = playerCharacter.Characteristics[0].Points},
                new Characteristic {CharacteristicImg = "pack://application:,,,/Images/characteristic/book.png", CharacteristicName = "Wisdom", Points = playerCharacter.Characteristics[1].Points},
                new Characteristic {CharacteristicImg = "pack://application:,,,/Images/characteristic/heart.png", CharacteristicName = "Honesty", Points = playerCharacter.Characteristics[2].Points},
                new Characteristic {CharacteristicImg = "pack://application:,,,/Images/characteristic/fox.png", CharacteristicName = "Cunning", Points = playerCharacter.Characteristics[3].Points},
            };

            Spells = playerCharacter.LearnedSpells;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
