﻿using DialogGame_HarryPotterSimulation.Characters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace DialogGame_HarryPotterSimulation.ViewModels
{
    public class GameWorldViewModel:BaseViewModel
    {
        private BaseViewModel selectedViewModel;
        private PlayerCharacter playerCharacter;

        public PlayerCharacter PlayerCharacter
        {
            get { return playerCharacter; }
            set
            {
                playerCharacter = value;
                OnPropertyChanged("PlayerCharacter");
            }
        }
        public BaseViewModel SelectedViewModel
        {
            get { return selectedViewModel; }
            set { selectedViewModel = value;
                OnPropertyChanged("SelectedViewModel");
            }
        }

        public ICommand UpdateViewCommand { get; set; } 

        public GameWorldViewModel(bool isLoaded)
        {
            playerCharacter = new PlayerCharacter();
            if (isLoaded)
                playerCharacter.RestoreGame();

            selectedViewModel = new BaseViewModel();
            UpdateViewCommand = new UpdateViewCommand(this,playerCharacter);
        }
        
    }
}
