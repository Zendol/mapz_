﻿using DialogGame_HarryPotterSimulation.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DialogGame_HarryPotterSimulation.Characters
{
    public class EnemyCharacter:Character,INotifyPropertyChanged
    {
        public string Name 
        { 
            get 
            { return name;  } 
            set 
            { 
                name = value;
                OnPropertyChanged("Name");
            } 
        }

        private string characterImg;
        public string CharacterImg
        {
            get { return characterImg; }
            set
            {
                characterImg = value;
                OnPropertyChanged("CharacterImg");
            }
        }


        private double healthPoints = 100;

        public double HP
        {
            get { return healthPoints; }
            set
            {
                healthPoints = value;
                OnPropertyChanged("HP");
            }
        }


        private List<Spell> spells;
        
        public List<Spell> Spells
        {
            get { return spells; }
            set
            {
                spells = value;
                OnPropertyChanged("Spells");
            }
        }

        public EnemyCharacter(int level, Mediator mediator)
        {
            this.name = Enemies.EnemiesCharacter[level].name;
            this.characterImg = Enemies.EnemiesCharacter[level].characterImg;
            this.spells = Enemies.EnemiesCharacter[level].spells;
            this.mediator = mediator;
        }

        public EnemyCharacter(string name, string characterImg,List<Spell> spells)
        {
            this.name = name;
            this.characterImg = characterImg;
            this.spells = spells;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string properyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(properyName));
        }
        public string DuelLog
        {
            get { return duelLog; }
            set
            {
                duelLog = value;
            }
        }
        public override void Notify(string message)
        {
            DuelLog += message;
        }
    }
}
