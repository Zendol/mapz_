﻿using DialogGame_HarryPotterSimulation.ViewModels;
using Lab1_Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialogGame_HarryPotterSimulation.Views
{
    /// <summary>
    /// Логика взаимодействия для SpellLearnView.xaml
    /// </summary>
    public partial class SpellLearnView : UserControl
    {
        private Point currentPoint = new Point();
        private SpellGame SpellGame;

        public SpellLearnView()
        {
            InitializeComponent();
            SpellGame = new SpellGame(new UnStartedSpellGame());
        }



        private void paintSurface_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
                currentPoint = e.GetPosition(this);
        }

        private void paintSurface_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Line line = new Line();
                line.Stroke = SystemColors.WindowFrameBrush;
                line.StrokeThickness = 5;
                line.X1 = currentPoint.X;
                line.Y1 = currentPoint.Y - 40;
                line.X2 = e.GetPosition(this).X;
                line.Y2 = e.GetPosition(this).Y - 40;
                currentPoint = e.GetPosition(this);
                paintSurface.Children.Add(line);
            }
        }

        private void StartGame()
        {
            AddStartButton();
            AddEndButton();
        }

        private void AddStartButton()
        {
            Point startPoint = PolyLine.Points.First();
            Button button = new Button();
            Canvas.SetLeft(button, startPoint.X);
            Canvas.SetTop(button, startPoint.Y);
            button.MouseEnter += new MouseEventHandler(startBtn_MouseEnter);
            button.Content = "Start";
            button.Foreground = new SolidColorBrush(Colors.Red);
            paintSurface.Children.Add(button);
        }

        private void AddEndButton()
        {
            Point endPoint = PolyLine.Points.Last();
            Button button = new Button();
            Canvas.SetLeft(button, endPoint.X);
            Canvas.SetTop(button, endPoint.Y);
            button.MouseEnter += new MouseEventHandler(endBtn_MouseEnter);
            button.Content = "End";
            button.Foreground = new SolidColorBrush(Colors.Red);
            button.SetBinding(Button.CommandProperty, new Binding("AddSpell"));
            paintSurface.Children.Add(button);
        }
        protected void endBtn_MouseEnter(object sender, EventArgs e)
        {
            SpellGame.Finish();
           
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.LeftUp);
        }

        protected void startBtn_MouseEnter(object sender, EventArgs e)
        {
            Button button = sender as Button;
            SpellGame.Start();
        }

        private void paintSurface_MouseLeave(object sender, MouseEventArgs e)
        {
            SpellGame.UnStart();
            paintSurface.Children.RemoveRange(3, paintSurface.Children.Count - 1);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            StartGame();
        }
    }
}
